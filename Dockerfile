FROM debian:buster-slim

ENV NGINX_VERSION=1.18.0
ENV PCRE_VERSION=8.44
ENV OPENSSL_VERSION=1.1.1g
ENV ZLIB_VERSION=1.2.11

RUN /bin/sh -c set -x     && \
    addgroup --system --gid 101 nginx && \
	adduser --system --disabled-login --ingroup nginx --no-create-home --home /nonexistent --gecos "nginx user" --shell /bin/false --uid 101 nginx && \
    apt update && apt update  && \
    apt  install build-essential wget -y && \
	wget https://nginx.org/download/nginx-$NGINX_VERSION.tar.gz && tar zxvf nginx-$NGINX_VERSION.tar.gz && \
	wget https://ftp.pcre.org/pub/pcre/pcre-$PCRE_VERSION.tar.gz && tar xzvf pcre-$PCRE_VERSION.tar.gz && \
	wget http://www.zlib.net/zlib-$ZLIB_VERSION.tar.gz && tar xzvf zlib-$ZLIB_VERSION.tar.gz && \
	wget https://www.openssl.org/source/openssl-$OPENSSL_VERSION.tar.gz && tar xzvf openssl-$OPENSSL_VERSION.tar.gz && \
	rm -rf *.tar.gz && \
	cd /nginx-$NGINX_VERSION && \
	./configure --prefix=/usr/share/nginx \
            --sbin-path=/usr/sbin/nginx \
            --modules-path=/usr/lib/nginx/modules \
            --conf-path=/etc/nginx/nginx.conf \
            --error-log-path=/var/log/nginx/error.log \
            --http-log-path=/var/log/nginx/access.log \
            --pid-path=/run/nginx.pid \
            --lock-path=/var/lock/nginx.lock \
            --user=www-data \
            --group=www-data \
            --build=Debian \
#            --http-client-body-temp-path=/var/lib/nginx/body \
#            --http-fastcgi-temp-path=/var/lib/nginx/fastcgi \
#            --http-proxy-temp-path=/var/lib/nginx/proxy \
#            --http-scgi-temp-path=/var/lib/nginx/scgi \
#            --http-uwsgi-temp-path=/var/lib/nginx/uwsgi \
            --with-openssl=../openssl-$OPENSSL_VERSION \
            --with-openssl-opt=enable-ec_nistp_64_gcc_128 \
            --with-openssl-opt=no-nextprotoneg \
            --with-openssl-opt=no-weak-ssl-ciphers \
            --with-openssl-opt=no-ssl3 \
            --with-pcre=../pcre-$PCRE_VERSION \
            --with-pcre-jit \
            --with-zlib=../zlib-$ZLIB_VERSION \
            --with-compat \
            --with-file-aio \
            --with-threads \
            --with-http_addition_module \
            --with-http_auth_request_module \
            --with-http_dav_module \
            --with-http_flv_module \
            --with-http_gunzip_module \
            --with-http_gzip_static_module \
            --with-http_mp4_module \
            --with-http_random_index_module \
            --with-http_realip_module \
            --with-http_slice_module \
            --with-http_ssl_module \
            --with-http_sub_module \
            --with-http_stub_status_module \
            --with-http_v2_module \
            --with-http_secure_link_module \
            --with-mail \
            --with-mail_ssl_module \
            --with-stream \
            --with-stream_realip_module \
            --with-stream_ssl_module \
            --with-stream_ssl_preread_module \
            --with-debug && \
		make && \
		make install && \
		cd .. && \
		rm -r nginx-$NGINX_VERSION openssl-$OPENSSL_VERSION pcre-$PCRE_VERSION zlib-$ZLIB_VERSION		
		
COPY ./nginx/nginx.conf /etc/nginx/nginx.conf
# RUN rm /etc/nginx/conf.d/default.conf
	 
EXPOSE 80
#EXPOSE 443

STOPSIGNAL SIGTERM

CMD ["nginx", "-g", "daemon off;"]